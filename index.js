var crypto = require('crypto');
var bigint = require('bignum');

const DEFAULTMODP = 'modp1';

module.exports = module.exports.createBespeke = bespeke;

function bespeke( password, algorithm ) {
	this._prime = crypto.getDiffieHellman(DEFAULTMODP).getPrime();
	if (password != undefined) {
		this.generateKeys(password, algorithm);
	}
}

bespeke.prototype.generateKeys = function(password, algorithm) {
	algorithm = algorithm || 'sha256';
	var primebigint = bigint.fromBuffer(this._prime);
	// Create the generator.
	var hash = crypto.createHash(algorithm);
	hash.update(password);
	var h = hash.digest();
	this._generator = bigint.fromBuffer(h).powm(bigint('2'), primebigint);
	// Create the private key.
	this._privateKey = crypto.randomBytes(this._prime.length);
	// Create the public key.
	this._publicKey = this._generator.powm(bigint.fromBuffer(this._privateKey), primebigint);
	return this.getPublicKey();
};
bespeke.prototype.secret = function(other_public_key, input_encoding, output_encoding) {
	if (input_encoding) {
		other_public_key = new Buffer(other_public_key, input_encoding);
	}
	var secret = bigint.fromBuffer(other_public_key).powm(bigint.fromBuffer(this._privateKey), bigint.fromBuffer(this._prime));
	var buf = secret.toBuffer();
	if (output_encoding) {
		return buf.toString(output_encoding);
	}
	return buf;
};
bespeke.prototype.getPrime = function(encoding) {
	var buf = this._prime;
	if (encoding) {
		return buf.toString(encoding);
	}
	return buf;
};
bespeke.prototype.getGenerator = function(encoding) {
	var buf = this._generator.toBuffer();
	if (encoding) {
		return buf.toString(encoding);
	}
	return buf;
};
bespeke.prototype.getPublicKey = function(encoding) {
	var buf = this._publicKey.toBuffer();
	if (encoding) {
		return buf.toString(encoding);
	}
	return buf;
};
bespeke.prototype.getPrivateKey = function(encoding) {
	var buf = this._privateKey.toBuffer();
	if (encoding) {
		return buf.toString(encoding);
	}
	return buf;
};

bespeke.prototype.sign = function( message ) {
	var hash = crypto.createHash('sha256').update(message).digest();
	var other = new bespeke( hash );
	var pubKey = other.getPublicKey();
	var secret = this.secret( pubKey );
	return Buffer.concat( [secret, pubKey] );
}

bespeke.prototype.verify = function( message, signature ) {
	var hash = crypto.createHash('sha256').update(message).digest();
	var other = new bespeke( hash );
	var sigSecret = signature.slice(0,96);
	var sigKey = signature.slice(96);
	var calcSecret = this.secret( sigKey );
	return (calcSecret.toString('hex') == sigSecret.toString('hex'));

}
