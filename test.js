var bespeke = require('./index.js');

const message = 'hello world';

var alice = new bespeke( message );
var bob = new bespeke( message );

var asecret = alice.secret( bob.getPublicKey() );
var bsecret = bob.secret( alice.getPublicKey() );

console.log('Public Key Length',alice.getPublicKey().length);
console.log('Secret Length',asecret.length);
console.log('Public Key',alice.getPublicKey().toString('hex'));

if (asecret.toString('hex') == bsecret.toString('hex')) {
	console.log('Success');
}




/*
 * Scenario - You are given a signature, and a public key. You must see if the public key, matched with the seignature creates a secret that was supplied.
 * 	Public Key - Target Secret - Signature
 */
